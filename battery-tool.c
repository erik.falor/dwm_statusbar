#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "proc.h"
#include "battery.h"

#ifdef SYSTEMD
# include "sdbus.h"
#endif

#define BUFSIZE 512

int
main (void)
{
	char out[BUFSIZE];

	memset(&out, '\0', BUFSIZE);

#ifdef SYSTEMD
	sd_init();
#endif

	getBatteryUrgency();
	printf("%s\n", getBatteryTime((char *)&out));
	printf("%s\n", getBatteryPercent((char *)&out));

#ifdef SYSTEMD
	sd_free();
#endif

	return 0;
}
