# Valid features are:
#   BATTERY     - read battery stats from /proc/acpi/battery/BAT0/state
#   SYS_BATTERY - read battery stats from /sys/class/power_supply/BAT0/
#   ALSA        - query ALSA for sound card volume
#   XKB         - query XKB for current X11 keyboard layout
#   FIFO        - allow for control through a FIFO file; the path is defined in fifo.h
#   DEBUG       - compile in debug mode; don't strip output binaries

FEATURES=XKB FIFO ALSA SYS_BATTERY

CFLAGS = -std=gnu99 -Wall -Wextra -pedantic $(addprefix -D,$(FEATURES))
STRIP=:
$(if $(findstring DEBUG,$(FEATURES)), \
	$(eval CFLAGS += -ggdb3) \
	$(eval STRIP  = :))

WMII_LDFLAGS = -lm -lixp $(LDFLAGS)
DWM_LDFLAGS = -lm $(LDFLAGS)
CC   = gcc
CSC  = csc
SUDO = sudo

OBJECTS = proc.o

$(if $(findstring ALSA,$(FEATURES)), \
	$(eval OBJECTS += alsavolume.o) \
	$(eval LDFLAGS += -lasound))

$(if $(findstring XKB,$(FEATURES)), \
	$(eval OBJECTS += xkb.o) \
	$(eval LDFLAGS += -lX11))

$(if $(findstring FIFO,$(FEATURES)), \
	$(eval OBJECTS += fifo.o))

$(if $(findstring SYSTEMD,$(FEATURES)), \
	$(eval OBJECTS += sdbus.o) \
	$(eval LDFLAGS += -lsystemd))

$(if $(findstring BATTERY,$(FEATURES)), \
	$(eval OBJECTS += battery.o))

$(if $(findstring SYS_BATTERY,$(FEATURES)), \
	$(eval OBJECTS += battery.o sys.o))

$(if $(findstring DEBUG,$(FEATURES)), \
	$(eval CSC_FLAGS=-O0 -k -C -ggdb3 $(addprefix -C -D,$(FEATURES)) $(addprefix -L ,$(LDFLAGS))), \
	$(eval CSC_FLAGS=-strip -O5 $(addprefix -C -D,$(FEATURES)) $(addprefix -L ,$(LDFLAGS))))


BINARIES = bus-client sdbus-tool wmii-statusbar dwm-statusbar battery-tool chicken-battery


.PHONY: all install clean deepclean tags

all: dwm-statusbar battery-tool

install: all
	$(SUDO) install -s -p -t /usr/local/bin dwm-statusbar
	$(SUDO) install -s -p -t /usr/local/bin battery-tool

clean:
	-rm -f *.o a.out wmii-statusbar dwm-statusbar chicken-battery battery-tool chicken-battery.c

deepclean: clean
	-rm -f tags cscope*.out

tags: *.c *.h
	ctags *.[ch]
	cscope -bq


bus-client: bus-client.c
	$(CC) $^ -o $@ $(CFLAGS) -lsystemd

sdbus-tool: sdbus-tool.c sdbus.o
	$(CC) $^ -o $@ $(CFLAGS) -lsystemd

wmii-statusbar.o: Makefile wmii-statusbar.h proc.h alsavolume.h
dwm-statusbar.o: Makefile dwm-statusbar.h proc.h alsavolume.h xkb.h statusbar.h fifo.h sdbus.h

wmii-statusbar: wmii-statusbar.c $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(WMII_LDFLAGS)

dwm-statusbar: dwm-statusbar.o $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(DWM_LDFLAGS)

battery-tool: battery-tool.c proc.o battery.o sys.o
	$(CC) -o $@ $^ $(CFLAGS) $(DWM_LDFLAGS)
	$(STRIP) $@

chicken-battery: chicken-battery.scm proc.o battery.o
	$(CSC) $(CSC_FLAGS) -o $@ $^
