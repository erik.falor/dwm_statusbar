#pragma once

// Return 1 if battery is going to die within 15 minutes,
// set global 'minutes' as a side-effect
int   getBatteryUrgency(void);

char* getBatteryTime(char *);
char* getBatteryPercent(char *);
