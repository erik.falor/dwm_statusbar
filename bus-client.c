/*
 gcc bus-client.c -o bus-client -lsystemd
 */
#include <stdio.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

int main() {
	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message *m = NULL;
	sd_bus *bus = NULL;
	int r;
	double p;

	/* Connect to the system bus */
	r = sd_bus_open_system(&bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to system bus: %s\n", strerror(-r));
		goto finish;
	}

	/* The following call to sd_bus_call_method() should be equivalent to this cmd:
		busctl call org.freedesktop.UPower             \
		/org/freedesktop/UPower/devices/battery_BAT0   \
		org.freedesktop.DBus.Properties Get            \
		ss org.freedesktop.UPower.Device Percentage
	*/
	r = sd_bus_get_property(bus,
			"org.freedesktop.UPower",                         /* service to contact */
			"/org/freedesktop/UPower/devices/battery_BAT0",   /* object path */
			"org.freedesktop.UPower.Device",                /* interface name */
			"Percentage",                                            /* method name */
			&error,                                           /* object to return error in */
			&m,                                               /* return message on success */
			"d"                                              /* input signature */
			);

	if (r < 0) {
		fprintf(stderr, "Failed to issue method call: %s\n", error.message);
		goto finish;
	}

	/* Parse the response message */

	r = sd_bus_message_read(m, "d", &p);
	if (r < 0) {
		fprintf(stderr, "Failed to parse response message: (%d) %s\n", r, strerror(-r));
		goto finish;
	}

	printf("Battery %f%%", p);

finish:
	sd_bus_error_free(&error);
	sd_bus_message_unref(m);
	sd_bus_unref(bus);

	return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
