#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "statusbar.h"

#if defined SYSTEMD

#include "sdbus.h"

#define CHARGING    1
#define DISCHARGING 2
#define FULL        4

char* getBatteryTime(char* buf) {
	uint32_t state,   *ss = &state;
	int64_t timeLeft, *tt = &timeLeft;
	int r;

	r = take_reading_sys(
			"org.freedesktop.UPower",
			"/org/freedesktop/UPower/devices/battery_BAT0",
			"org.freedesktop.UPower.Device",
			"State",
			"u",
			(void*)&ss);

	if (r < 0) {
		fprintf(stderr, "[%s:%d] getBatteryTime(0): [err=%d] %s", __FILE__, __LINE__, r, sd_errstr);
		free(sd_errstr);
	}
	else if (state == FULL) {
		snprintf(buf, SBAR, "Bat:FULL");
	}
	else {
		r = take_reading_sys(
				"org.freedesktop.UPower",
				"/org/freedesktop/UPower/devices/battery_BAT0",
				"org.freedesktop.UPower.Device",
				state == CHARGING ? "TimeToFull" : "TimeToEmpty",
				"x",
				(void*)&tt);

		if (r < 0) {
			fprintf(stderr, "[%s:%d] getBatteryTime(1): [err=%d] %s", __FILE__, __LINE__, r, sd_errstr);
			free(sd_errstr);
		}
		else {
			int h, m;
			h = timeLeft / 3600;
			m = timeLeft % 3600 / 60;
			snprintf(buf, SBAR, "Bat:%d.%02d%c",
					h, m, state == DISCHARGING ? '-' : '+');
		}
	}

	return buf;
}

char* getBatteryPercent(char* buf) {
	uint32_t state, *ss = &state;
	double percent, *pp = &percent;
	int r;

	r = take_reading_sys(
			"org.freedesktop.UPower",
			"/org/freedesktop/UPower/devices/battery_BAT0",
			"org.freedesktop.UPower.Device",
			"State",
			"u",
			(void*)&ss);

	if (r < 0) {
		fprintf(stderr, "[%s:%d] getBatteryPercent(0): [err=%d] %s", __FILE__, __LINE__, r, sd_errstr);
		free(sd_errstr);
	}
	else {
		r = take_reading_sys(
				"org.freedesktop.UPower",
				"/org/freedesktop/UPower/devices/battery_BAT0",
				"org.freedesktop.UPower.Device",
				"Percentage",
				"d",
				(void*)&pp);

		if (r < 0) {
			fprintf(stderr, "[%s:%d] getBatteryPercent(1): [err=%d] %s", __FILE__, __LINE__, r, sd_errstr);
			free(sd_errstr);
		}
		else {
			switch (state) {
				case CHARGING:
					snprintf(buf, SBAR, "Bat:%.0f%%+", percent);
					break;
				case DISCHARGING:
					snprintf(buf, SBAR, "Bat:%.0f%%-", percent);
					break;
				case FULL:
					snprintf(buf, SBAR, "Bat:%.0f%%", percent);
					break;
			}
		}
	}

	return buf;
}


#elif defined SYS_BATTERY

#include "sys.h"

static char rbuf[READBUFFER];
static char present, direction;
static int hours;
static double rate;
int minutes;


// Files under the /sys directory are explained here:
// /usr/src/linux/Documentation/ABI/testing/sysfs-class-power
//
// See also:
// https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power
//
// Compare to
// % upower -i /org/freedesktop/UPower/devices/battery_BAT0

// Return 1 if battery is going to die within 15 minutes,
// set global 'minutes' as a side-effect
int getBatteryUrgency(void) {
	int urgent = 0;

	readSysFile("class/power_supply/BAT0/present", rbuf);

	/* is battery present? */
	if ((present = rbuf[0]) == '1') {
		/* is battery discharging? */
		readSysFile("class/power_supply/BAT0/status", rbuf);
		direction = rbuf[0] == 'D' ? '-' : '+';

        readSysFile("class/power_supply/BAT0/energy_now", rbuf);
        double energy_now = atof(rbuf);

        readSysFile("class/power_supply/BAT0/power_now", rbuf);
        double power_now = atof(rbuf);
        double hours_left;

		if (direction == '-') {
			/* How long until battery is depleted?
			   charge/discharge rate  =  energy_now / power_now */

            if (power_now > 1e-5) {
                rate = hours_left = energy_now / power_now;
                urgent = hours_left < 0.25;
            }
            else
                rate = hours_left = 0.0;
		}
        else {
			/* How long until battery is charged?
               (POWER_SUPPLY_ENERGY_FULL - POWER_SUPPLY_ENERGY_NOW) / POWER_SUPPLY_POWER_NOW
               */
			readSysFile("class/power_supply/BAT0/energy_full", rbuf);
			double energy_full = atof(rbuf);
            if (power_now > 1e-5)
                rate = hours_left = (energy_full - energy_now) / power_now;
            else
                rate = hours_left = 0.0;
        }

        hours = (int)hours_left;
        minutes = (int)(hours_left * 60.0) % 60;
	}

	return urgent;
}

char* getBatteryTime(char *buf) {
	if (present == '0')
		snprintf(buf, SBAR, "ACpwr");
	else
		if (rate <= 1e-5)
			snprintf(buf, SBAR, "Bat:FULL%c", direction);
		else
			snprintf(buf, SBAR, "Bat:%d.%02d%c",
					hours,
					minutes,
					direction);

	return buf;
}

char* getBatteryPercent(char* buf) {
    if (present == '0') {
        snprintf(buf, SBAR, "ACpwr");
    }
    else {
        readSysFile("class/power_supply/BAT0/capacity", rbuf);
        char capacity[4];
        strncpy(capacity, rbuf, 4);
        char *c = strchr(capacity, '\n');
        if (c)
            *c = '\0';
        else
            capacity[3] = '\0';
        snprintf(buf, SBAR, "Bat:%s%%%c", capacity, direction);
    }

	return buf;
}


#else

#include "proc.h"

static char procbuffer[READBUFFER];


static char *present, direction;
static int rate, remaining, capacity, hours;
int minutes;


// Return 1 if battery is going to die within 15 minutes,
// set global 'minutes' as a side-effect
int getBatteryUrgency(void) {
	int urgent = 0;
	char *c;

	readProcFile("acpi/battery/BAT0/state", procbuffer);

	/* printf("read in:\n===\n%s\n===\n\n", procbuffer); */ /*DELETE ME*/

	/* is battery present? */
	c = strtok(procbuffer, ": ");
	present = strtok(NULL, ": ");
	/* printf("PRESENT: %s\n", present); */ /* DELETE ME */
	/* printf("PRESENT? %c\n\n", *present == 'y' ? 'y' : 'n'); */ /* DELETE ME */

	if (*present == 'y') {
		/* discharging? */
		c = strtok(NULL, ":");
		c = strtok(NULL, ":");
		c = strtok(NULL, " \n");
		if (c) {
			direction = *c == 'd' ? '-' : '+';
			//printf("DISCHARGING:\n===\n%s\n===\n\n", direction); /* DELETE ME */
		}

		/* rate */
		c = strtok(NULL, ":");
		c = strtok(NULL, ": ");
		rate = atoi(c);

		if (rate) {
			/* remaining capacity, mA minutes */
			c = strtok(NULL, ":");
			remaining = 60 * atoi(strtok(NULL, " "));
			//printf("REMAINING:\n===\n%d\n===\n\n", remaining); /* DELETE ME */

			/* now read from info the last full capacity, converted to mA minutes */
			readProcFile("acpi/battery/BAT0/info", procbuffer);
			c = strtok(procbuffer, "\n");
			c = strtok(NULL, "\n");
			c = strtok(NULL, ":");
            char *c_capacity = strtok(NULL, " ");
            if (c_capacity)
                capacity = 60 * atoi(c_capacity);

			if (direction == '-')
				minutes = remaining / rate;
			else
				minutes = (capacity - remaining) / rate;

			hours = minutes / 60;
			minutes %= 60;

			urgent = direction == '-' && hours == 0 && minutes <= 15;
		}
	}

	return urgent;
}

char* getBatteryTime(char *buf) {
	if (rate) {
		if (*present == 'y') {
			if (0 == rate) {
				snprintf(buf, SBAR, "Bat:FULL%c", direction);
			}
			else {
				snprintf(buf, SBAR, "Bat:%d.%02d%c",
						hours,
						minutes,
						direction);
			}
		}
		else {
			snprintf(buf, SBAR, "ACpwr");
		}
	}
	else if (direction == '+') {
		snprintf(buf, SBAR, "FULL");
	}
	else {
		snprintf(buf, SBAR, "N/A");
	}

	return buf;
}

char* getBatteryPercent(char* buf) {
	if (rate) {
		if (*present == 'y') {
			snprintf(buf, SBAR, "Bat:%.0f%%%c",
					(double)remaining / (double)capacity * 100.0,
					direction);
		}
		else {
			snprintf(buf, SBAR, "ACpwr");
		}
	}
	else if (direction == '+') {
		snprintf(buf, SBAR, "FULL");
	}
	else {
		snprintf(buf, SBAR, "N/A");
	}

	return buf;
}

#endif
