#include <stdio.h>
#include <time.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>

#include "statusbar.h"

/*
 * Read READBUFFER bytes from the specified /sys file
 * into *uf, 
 * also return pointer to buf
 */
char* readSysFile(const char* file, char* buf) {
	static FILE *info;
	static char syspath[PATH_MAX];

	strncpy((char*)&syspath, "/sys/", PATH_MAX);
	strncat((char*)&syspath, file, PATH_MAX - strlen(syspath));

	if (NULL == (info = fopen((char*)&syspath, "r")))
		error_at_line(1, errno, __FILE__, __LINE__, "fopen(%s) FAIL", syspath);

	(void) fread(buf, sizeof(char), READBUFFER, info);
	if (ferror(info))
		error_at_line(1, errno, __FILE__, __LINE__, "ferror(%s) FAIL", syspath);

	if (-1 == fclose(info))
		error_at_line(1, errno, __FILE__, __LINE__, "fclose(%s) FAIL", syspath);

	return buf;
}
