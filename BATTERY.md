Querying battery state with upowerd
===================================

% upower -i /org/freedesktop/UPower/devices/battery_BAT0
  native-path:          BAT0
  vendor:               SMP
  model:                5B10W13931
  serial:               1057
  power supply:         yes
  updated:              Sat 02 Jan 2021 04:22:15 PM MST (94 seconds ago)
  has history:          yes
  has statistics:       yes
  battery
    present:             yes
    rechargeable:        yes
    state:               charging
    warning-level:       none
    energy:              42.54 Wh
    energy-empty:        0 Wh
    energy-full:         52.88 Wh
    energy-full-design:  51.01 Wh
    energy-rate:         32.416 W
    voltage:             17.261 V
    time to full:        19.1 minutes
    percentage:          80%
    capacity:            100%
    technology:          lithium-polymer
    icon-name:          'battery-full-charging-symbolic'
  History (charge):
    1609629735	80.000	charging
  History (rate):
    1609629735	32.416	charging



Trolling the D-Bus for cool data
================================

% busctl call org.freedesktop.UPower /org/freedesktop/UPower/devices/battery_BAT0 org.freedesktop.DBus.Properties Get ss org.freedesktop.UPower.Device Percentage
v d 43

% busctl tree org.freedesktop.UPower
└─/org
  └─/org/freedesktop
    └─/org/freedesktop/UPower
      ├─/org/freedesktop/UPower/Wakeups
      └─/org/freedesktop/UPower/devices
        ├─/org/freedesktop/UPower/devices/DisplayDevice
        ├─/org/freedesktop/UPower/devices/battery_BAT0
        └─/org/freedesktop/UPower/devices/line_power_AC



% busctl introspect org.freedesktop.UPower /org/freedesktop/UPower/devices/battery_BAT0   
NAME                                TYPE      SIGNATURE RESULT/VALUE            FLAGS
org.freedesktop.DBus.Introspectable interface -         -                       -
.Introspect                         method    -         s                       -
org.freedesktop.DBus.Peer           interface -         -                       -
.GetMachineId                       method    -         s                       -
.Ping                               method    -         -                       -
org.freedesktop.DBus.Properties     interface -         -                       -
.Get                                method    ss        v                       -
.GetAll                             method    s         a{sv}                   -
.Set                                method    ssv       -                       -
.PropertiesChanged                  signal    sa{sv}as  -                       -
org.freedesktop.UPower.Device       interface -         -                       -
.GetHistory                         method    suu       a(udu)                  -
.GetStatistics                      method    s         a(dd)                   -
.Refresh                            method    -         -                       -
.Capacity                           property  d         96.9464                 emits-change
.Energy                             property  d         38.1507                 emits-change
.EnergyEmpty                        property  d         0                       emits-change
.EnergyFull                         property  d         61.9158                 emits-change
.EnergyFullDesign                   property  d         62.16                   emits-change
.EnergyRate                         property  d         13.3533                 emits-change
.HasHistory                         property  b         true                    emits-change
.HasStatistics                      property  b         true                    emits-change
.IconName                           property  s         "battery-full-symbolic" emits-change
.IsPresent                          property  b         true                    emits-change
.IsRechargeable                     property  b         true                    emits-change
.Luminosity                         property  d         0                       emits-change
.Model                              property  s         "BAT"                   emits-change
.NativePath                         property  s         "BAT0"                  emits-change
.Online                             property  b         false                   emits-change
.Percentage                         property  d         61                      emits-change
.PowerSupply                        property  b         true                    emits-change
.Serial                             property  s         "0001"                  emits-change
.State                              property  u         2                       emits-change
.Technology                         property  u         1                       emits-change
.Temperature                        property  d         0                       emits-change
.TimeToEmpty                        property  x         10285                   emits-change
.TimeToFull                         property  x         0                       emits-change
.Type                               property  u         2                       emits-change
.UpdateTime                         property  t         1484187778              emits-change
.Vendor                             property  s         "Notebook"              emits-change
.Voltage                            property  d         11.375                  emits-change
.WarningLevel                       property  u         1                       emits-change

