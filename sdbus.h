#pragma once

#include <systemd/sd-bus.h>

int sd_init();

void sd_free();

int take_reading_sys(const char *destination, const char *path,
		const char *interface, const char *member, const char *type, void** output);

int take_reading_usr(const char *destination, const char *path,
		const char *interface, const char *member, const char *type, void** output);

extern char *sd_errstr;
