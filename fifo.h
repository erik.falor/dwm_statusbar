#ifndef FIFO_H

#define FIFO_PATH "/tmp/dwm-statusbar"
#define FIFO_BUFSZ 80


int fifoInit(void);
char* fifoCheck(void);
int fifoFree(void);


#define FIFO_H
#endif
