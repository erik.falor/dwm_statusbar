/*
	gcc -std=gnu99 -c sdbus.c
	gcc sdbus-tool.c sdbus.o -o sdbus-tool -std=gnu99 -lsystemd
 */

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "sdbus.h"

typedef enum {
	STATE,
	PERCENTAGE,
	TIME_LEFT,
	IS_PRESENT,
	THING_END,
} stuff_t;


int main() {
	int r, i;
	double percent, *pp = &percent;
	int seconds, *tt = &seconds,
		present, *bb = &present;
	uint32_t state, *ss = &state;

	sd_init();

	for (i = 0; i < 21; ++i) {
		switch (i % THING_END) {
			case STATE:
				{
					printf("doing STATE\n");
					r = take_reading_sys(
							"org.freedesktop.UPower",                         /* service to contact */
							"/org/freedesktop/UPower/devices/battery_BAT0",   /* object path */
							"org.freedesktop.UPower.Device",                /* interface name */
							"State",                                            /* method name */
							"u",                                             /* input signature */
							(void*)&ss
							);

					if (r < 0) {
						fprintf(stderr, "%s", sd_errstr);
						free(sd_errstr);
					}
					else {
						printf("Battery state %u\n", state);
					}
				}
				break;

			case PERCENTAGE:
				{
					printf("doing PERCENTAGE\n");
					r = take_reading_sys(
							"org.freedesktop.UPower",                         /* service to contact */
							"/org/freedesktop/UPower/devices/battery_BAT0",   /* object path */
							"org.freedesktop.UPower.Device",                /* interface name */
							"Percentage",                                            /* method name */
							"d",                                             /* input signature */
							(void*)&pp
							);

					if (r < 0) {
						fprintf(stderr, "%s", sd_errstr);
						free(sd_errstr);
					}
					else {
						printf("Battery %.2f%%\n", percent);
					}
				}
				break;

			case TIME_LEFT:
				{
					printf("doing TIME_LEFT\n");
					r = take_reading_sys(
							"org.freedesktop.UPower",                         /* service to contact */
							"/org/freedesktop/UPower/devices/battery_BAT0",   /* object path */
							"org.freedesktop.UPower.Device",                /* interface name */
							"TimeToEmpty",                                            /* method name */
							"x",                                             /* input signature */
							(void*)&tt
							);

					if (r < 0) {
						fprintf(stderr, "%s", sd_errstr);
						free(sd_errstr);
					}
					else {
						int h, m, s;
						h = seconds / 3600;
						seconds %= 3600;
						m = seconds / 60;
						s = seconds % 60;
						if (h)
							printf("Time to empty %d:%02d:%02d\n", h, m, s);
						else
							printf("Time to empty %d:%02d\n", m, s);
					}
				}
				break;

			case IS_PRESENT:
				{
					printf("doing IS_PRESENT\n");
					r = take_reading_sys(
							"org.freedesktop.UPower",                         /* service to contact */
							"/org/freedesktop/UPower/devices/battery_BAT0",   /* object path */
							"org.freedesktop.UPower.Device",                /* interface name */
							"IsPresent",                                            /* method name */
							"b",                                             /* input signature */
							(void*)&bb
							);

					if (r < 0) {
						fprintf(stderr, "%s", sd_errstr);
						free(sd_errstr);
					}
					else {
						printf("IsPresent = %s\n", present ? "true" : "false");
					}
				}
				break;

			default:
				printf("how did I end up here?\n");
				break;
		}

		sleep(2);
	}

	sd_free();

	return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
