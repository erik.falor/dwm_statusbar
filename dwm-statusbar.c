#include <stdio.h>
#include <time.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <signal.h>

#include "proc.h"

#ifdef XKB
# include "xkb.h"
#endif

#ifdef ALSA
# include "alsavolume.h"
#endif

#ifdef FIFO
# include "fifo.h"
#endif

#if defined BATTERY || defined SYS_BATTERY
# include "battery.h"
#endif

#ifdef SYSTEMD
# include "sdbus.h"
#endif

#include "statusbar.h"
#include "dwm-statusbar.h"

Display *dpy = NULL;
static int loadAve = 0;
static int timerMode = 0;
static int timerSecs = 0;
static int overtime = 0;
static time_t when = 0;
static int paused = 1;
static int timer_duration = 50 * 60; // 50 minutes

#if defined BATTERY || defined SYS_BATTERY
static int batteryPercent = 0;
#endif

/*
 * toggle the presentation of certain datas upon receipt of signals
 * Note: the signal handler interrupts sleep - and so will steal up to a second from the timer!
 */
void togglePresentation(int signum) {
	if (signum == SIGUSR2)
		loadAve ^= 1;

#if defined BATTERY || defined SYS_BATTERY
	else if (signum == SIGUSR1)
		batteryPercent ^= 1;
#endif

	else if (signum == SIGHUP)
		// display the countdown timer when MOD4+colon are pressed
		timerMode ^= 1;

	else if (signum == SIGWINCH)
		// if the backtick is signaled, note the time...
		when = time(NULL);

	else if (signum == SIGURG)
		// pause the timer
		paused ^= 1;

	else if (signum == SIGQUIT && time(NULL) - when < 2) {
		// only reset the time if this signal happens within a second of SIGWINCH
		timerSecs = timer_duration;
		overtime = 0;
	}

	else if (signum == SIGQUIT)
		// SIGQUIT subtracts a minute
		timerSecs -= 59;

	else if (signum == SIGPOLL)
		// SIGPOLL adds one minute to the timer
		timerSecs += 61;
}

/*
 * get date & time
 */
char* getDateTime(char* buf) {
	static time_t curtime;
	static struct tm *loctime;

	curtime = time(NULL);
	loctime = localtime(&curtime);
	strftime(buf, SBAR, TIME_FMT, loctime);
	return buf;
}

void
setstatus(char *str)
{
#ifdef DEBUG
	puts(str);
#else
	XStoreName(dpy, DefaultRootWindow(dpy), str);
	XSync(dpy, False);
#endif
}

#ifdef FIFO
static void cmdParse(char* cmds) {
	char *c = strtok(cmds, " \n\t");
	do {
		if (c) {
			if      (!strcasecmp(c, "loadave"))
				loadAve ^= 1;
			else if (!strcasecmp(c, "toggle"))
				timerMode ^= 1;
			else if (!strcasecmp(c, "timer"))
				timerMode = 1;
			else if (!strcasecmp(c, "clock"))
				timerMode = 0;
			else if (!strcasecmp(c, "stats"))
				timerMode = 0;
			else if (!strcasecmp(c, "reset")) {
				timerSecs = timer_duration;
				overtime = 0;
			}
			else if (!strcasecmp(c, "pause"))
				paused ^= 1;
			else if (!strcasecmp(c, "stop"))
				paused = 1;
			else if (!strcasecmp(c, "resume") || !strcasecmp(c, "start"))
				paused = 0;
			else if (*c == '+')
				timerSecs += (60 * atoi((const char*)++c)) + 1;
			else if (*c == '-')
				timerSecs -= (60 * atoi((const char*)++c)) + 1;
			else if (*c == '=') {
				// by default, regard =N to mean N minutes
				// A suffix can convert N into seconds or hours
				int conversion_factor = 60;
				if (strchr(c, 's'))
					conversion_factor = 1;
				else if (strchr(c, 'h'))
					conversion_factor = 3600;
				timer_duration = atoi((const char*)++c);
				timerSecs = timer_duration = timer_duration * conversion_factor + 1;
			}
#if defined BATTERY || defined SYS_BATTERY
			else if (!strcasecmp(c, "battery"))
				batteryPercent ^= 1;
#endif

		}
	} while (NULL != (c = strtok(NULL, " \n\t")));
}
#endif

int
main(void)
{
	char out[SBAR + 1];
	char *buf; 

#ifdef FIFO
	char *fifoCmd;
	fifoInit();
#endif

#ifdef SYSTEMD
	sd_init();
#endif

	if (!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "XOpenDisplay() FAIL\n");
		return 1;
	}

#define INSTALLSIGNAL(S, F) { if (SIG_ERR == signal(S, F)) \
	error_at_line(1, errno, __FILE__, __LINE__, "signal(" #S ") FAIL"); }

	INSTALLSIGNAL(SIGUSR2,  togglePresentation);
	INSTALLSIGNAL(SIGHUP,   togglePresentation);
	INSTALLSIGNAL(SIGQUIT,  togglePresentation);
	INSTALLSIGNAL(SIGWINCH, togglePresentation);
	INSTALLSIGNAL(SIGURG,   togglePresentation);
	INSTALLSIGNAL(SIGPOLL,  togglePresentation);
#if defined BATTERY || defined SYS_BATTERY
	INSTALLSIGNAL(SIGUSR1,  togglePresentation);
#else
	INSTALLSIGNAL(SIGUSR1,  SIG_IGN);
#endif

	if (NULL == (buf = (char*)malloc(sizeof(char) * READBUFFER)))
		error_at_line(1, errno, __FILE__, __LINE__, "malloc() FAIL");

	memset(&out, '\0', (size_t)SBAR);

	setstatus(PROGNAM " " VERSION);

	for (;; *out = '\0', sleep(SLEEPYTIME) ) {

#ifdef FIFO
		fifoCmd = fifoCheck();
		if (*fifoCmd)
			cmdParse(fifoCmd);
#endif

		if (!paused) {
			if (overtime)
				timerSecs++;
			else if (timerSecs == 0)
				timerSecs = overtime = 1;
			else
				timerSecs--;
		}

#if defined BATTERY || defined SYS_BATTERY
		if (getBatteryUrgency()) {
			extern int minutes;
			snprintf(out, (size_t)SBAR,
					 "--==[ Battery is dying in %d minutes! ]==--",
					 minutes);
			setstatus(out);
		}
		else {
#endif
			if (timerMode) {
				snprintf(out, (size_t)SBAR,
						paused
						? "(%02d:%02d)"
						: "%02d:%02d",
						timerSecs / 60, timerSecs % 60);
			}
			else {
				/*
				 * get wifi state, if wifi is on - icons?
				 * which font had the hearts and stuff?
				 */
				//strncat(out, getWifi(buf), (size_t)SBAR);
				//strncat(out, " | ", (size_t) SBAR);

#ifdef XKB
				{
					int numlock = get_numlock_state();
					if (numlock < 0)
						strncat(out, "[NumLk?] ", (size_t)SBAR);
					else if (numlock > 0)
						strncat(out, "[NumLk] ", (size_t)SBAR);
				}
#endif

#ifdef ALSA
				strncat(out, "Vol:", (size_t)SBAR);
				char* v = getAlsaVolume("Master");
				if (v)
					strncat(out, v, (size_t)SBAR);
				else
					strncat(out, "Ø", (size_t)SBAR);
				strncat(out, " ", (size_t) SBAR);
#endif

#ifdef XKB
				strncat(out, "Kb:", (size_t)SBAR);
				strncat(out, xkbGetGroup(buf, (size_t) 2), (size_t)SBAR);
				strncat(out, " ", (size_t) SBAR);
#endif

				strncat(out, getCPU(buf), (size_t)SBAR);
				strncat(out, " ", (size_t) SBAR);

				strncat(out, getMemory(buf, 0), (size_t) SBAR);
				strncat(out, " ", (size_t) SBAR);

#if defined BATTERY || defined SYS_BATTERY
				if (batteryPercent == 1)
					strncat(out, getBatteryPercent(buf), (size_t) SBAR);
				else
					strncat(out, getBatteryTime(buf), (size_t) SBAR);
				strncat(out, " ", (size_t) SBAR);
#endif

				if (loadAve == 1) {
					strncat(out, "|", (size_t) SBAR);
					strncat(out, getLoadAve(buf), (size_t) SBAR);
					strncat(out, "|", (size_t) SBAR);
				}
				else
					strncat(out, getDateTime(buf), (size_t) SBAR);
			}
			setstatus(out);

#if defined BATTERY || defined SYS_BATTERY
		}
#endif

	}

#ifdef SYSTEMD
	sd_free();
#endif
}

/* vim: set noexpandtab: */
