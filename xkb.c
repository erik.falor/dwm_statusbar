#include <stdio.h>

#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/Xlibint.h>
#include <X11/Xatom.h>

#include "statusbar.h"


// static Display *dpy = NULL;
extern Display *dpy;

int xkbInit(void) {
	int  xkbEventType, xkbError, reason_rtrn, mjr, mnr;
	char *display_name = (char *)NULL;

	/* Lets begin */
	if (!dpy) {
		mjr = XkbMajorVersion;
		mnr = XkbMinorVersion;
		dpy = XkbOpenDisplay(display_name, &xkbEventType, &xkbError,
				&mjr, &mnr, &reason_rtrn);
	}

    return 0;
}


char* xkbGetGroup(char* buf, int len) {
	XkbStateRec xkbstate;
	static XkbDescPtr xkbdesc = NULL;

	if (dpy == NULL) {
		strcpy(buf, "?");
		return buf;
	}

	if (Success != XkbGetState(dpy, XkbUseCoreKbd, &xkbstate)) {
		strcpy(buf, "?");
		return buf;
	}

	if (!xkbdesc)
		xkbdesc = XkbAllocKeyboard();

	if (!xkbdesc) {
		strcpy(buf, "?");
		return buf;
	}

	/* get the names of the layout */
	if ( Success == XkbGetNames(dpy, 1<<12, xkbdesc)) {
		Atom iatoms[4];
		char *iatomnames[4];
		int i, j;

		for (i = 0, j = 0; i < 4; i++)
			if (xkbdesc->names->groups[i] != None)
				iatoms[j++] = xkbdesc->names->groups[i];

		if (XGetAtomNames(dpy, iatoms, j, iatomnames))
			buf = strndup(iatomnames[xkbstate.locked_group], len);
	}

	return buf;
}

int get_numlock_state() {
    Window dummy1, dummy2;
    int dummy3, dummy4, dummy5, dummy6;

    if (dpy == NULL)
        return -1;

    static XModifierKeymap* map = NULL;
    if (!map)
        map = XGetModifierMapping(dpy);

    KeyCode numlock_keycode = XKeysymToKeycode(dpy, XK_Num_Lock);

    if (numlock_keycode == NoSymbol)
        return -1;

    int numlock_mask = 0;
    for (int i = 0; i < 8; ++i) {
        if (map->modifiermap[map->max_keypermod * i] == numlock_keycode)
            numlock_mask = 1 << i;
    }

    unsigned int mask;
    XQueryPointer(dpy, DefaultRootWindow(dpy),
            &dummy1, &dummy2, &dummy3, &dummy4, &dummy5, &dummy6,
            &mask);

    return mask & numlock_mask;
}
