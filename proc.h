#pragma once

char* readProcFile(const char* file, char* buf);

char* getLoadAve(char* buf);
char* getCPU(char* buf);
char* getMemory(char* buf, int freeMem);
