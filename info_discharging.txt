present:                 yes
design capacity:         3000 mAh
last full capacity:      2837 mAh
battery technology:      rechargeable
design voltage:          15120 mV
design capacity warning: 300 mAh
design capacity low:     0 mAh
cycle count:		  0
capacity granularity 1:  64 mAh
capacity granularity 2:  64 mAh
model number:            BAT
serial number:           0001
battery type:            LION
OEM info:                Notebook

