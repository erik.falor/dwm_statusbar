#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>
#include <unistd.h>

sd_bus *u_bus = NULL,
	   *s_bus = NULL;

char *sd_errstr;


/* connect to both of the buses and get stuff ready */
int sd_init(void) {
	int r;

	r = sd_bus_open_user(&u_bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to user bus: %s\n", strerror(-r));
		u_bus = NULL;
	}
	else if ((r = sd_bus_open_system(&s_bus)) < 0) {
		fprintf(stderr, "Failed to connect to system bus: %s\n", strerror(-r));
		sd_bus_unref(u_bus);
		s_bus = NULL;
	}

	return r;
}

void sd_free(void) {
	if (u_bus)
		sd_bus_unref(u_bus);
	if (s_bus)
		sd_bus_unref(s_bus);
}


static int take_reading(sd_bus *bus, const char *destination, const char *path,
		const char *interface, const char *member, const char *type, void** output) {

	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message *m = NULL;
	int r;

	r = sd_bus_get_property(bus, destination, path, interface, member, &error, &m, type);

	if (r < 0) {
		asprintf(&sd_errstr, "Failed to issue method call: %s\n", error.message);
	}
	else {
		/* Parse the response message */
		r = sd_bus_message_read(m, type, *output);
		if (r < 0) {
			asprintf(&sd_errstr, "Failed to parse response message: (%d) %s\n", r, strerror(-r));
		}
	}
	sd_bus_error_free(&error);
	sd_bus_message_unref(m);

	return r;
}


int take_reading_sys(const char *destination, const char *path,
		const char *interface, const char *member, const char *type, void** output) {
	return take_reading(s_bus, destination, path, interface, member, type, output);
}

int take_reading_usr(const char *destination, const char *path,
		const char *interface, const char *member, const char *type, void** output) {
	return take_reading(u_bus, destination, path, interface, member, type, output);
}
