#include <stdio.h>
#include <time.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>

#include "statusbar.h"

/*
 * Where proc files are located
 */
#define PROC "/proc/"


/*
 * Read READBUFFER bytes from the specified /proc file
 * into *uf, 
 * also return pointer to buf
 */
char* readProcFile(const char* file, char* buf) {
	static FILE *info;
	static char procpath[PATH_MAX];

	strcpy((char*)&procpath, PROC);
	strcat((char*)&procpath, file);

	if (NULL == (info = fopen((char*)&procpath, "r"))) {
		error_at_line(1, errno, __FILE__, __LINE__, "fopen(%s) FAIL", file);
	}

	(void) fread(buf, sizeof(char), READBUFFER, info);
	if (ferror(info)) {
		error_at_line(1, errno, __FILE__, __LINE__, "ferror(%s) FAIL", file);
	}

	if (-1 == fclose(info)) {
		error_at_line(1, errno, __FILE__, __LINE__, "fclose(%s) FAIL", file);
	}

	return buf;
}


/*
 * get load average
 */
char* getLoadAve(char* buf) {
	char* c = readProcFile("loadavg", buf);
	for (int i = 0; i <= 2; i++) {
		c = strchr(c, ' ') + 1;
	}
	*--c = '\0';
	return buf;
}

/*
 * get CPU%
 *
 * in a loop:
 *  read /proc/stat for line beginning with "cpu"
 *  save the first four columns of numbers: they correspond to
 *  ticks spent in:
 *       user   nice system idle
 *  cpu  300745 9636 211625 3366592 44847 57 1811 0 0 0
 *
 *  take the difference between the fresh values & last times'
 *  apply this formula:
 *  %cpu = (user+system)/(user+system+nice+idle)
 */
char* getCPU(char* buf) {
	static unsigned int user = 0,
						nice = 0,
						system = 0,
						idle = 0,
						prvUser = 0,
						prvNice = 0,
						prvSystem = 0,
						prvIdle = 0,
						difUser = 0,
						difNice = 0,
						difSystem = 0,
						difIdle = 0;

	readProcFile("stat", buf);
	strtok(buf, " ");                 /* throw away cpu label */

	if (0 == prvUser) {
		prvUser = atoi(strtok(NULL, " "));   /* get user ticks */
		prvNice = atoi(strtok(NULL, " "));   /* get nice ticks */
		prvSystem = atoi(strtok(NULL, " ")); /* get system ticks */
		prvIdle = atoi(strtok(NULL, " "));   /* get idle ticks */
		strcpy(buf, "CPU:N/A");
	}
	else {
		user = atoi(strtok(NULL, " "));   /* get user ticks */
		nice = atoi(strtok(NULL, " "));   /* get nice ticks */
		system = atoi(strtok(NULL, " ")); /* get system ticks */
		idle = atoi(strtok(NULL, " "));   /* get idle ticks */

		difUser = user - prvUser;
		difNice = nice - prvNice;
		difSystem = system - prvSystem;
		difIdle = idle - prvIdle;

		prvUser = user;
		prvNice = nice;
		prvSystem = system;
		prvIdle = idle;

		if (0 == (difUser + difSystem + difNice + difIdle)) {
			strcpy(buf, "CPU:N/A");
		}
		else {
			sprintf(buf, "CPU:%d%%",
						  100 * (difUser + difSystem)
									/
				  (difUser + difSystem + difNice + difIdle));
		}
	}
	return buf;
}

/*
 * get memory state - swap, etc
 * read lines from /proc/meminfo:

	MemTotal:       32833164 kB
	MemFree:        22989700 kB
	MemAvailable:   28960000 kB
	Buffers:          113316 kB
	Cached:          6775912 kB
	SwapCached:            0 kB
	Active:          3266352 kB
	Inactive:        4562836 kB
	Active(anon):     941764 kB
	Inactive(anon):   767920 kB
	Active(file):    2324588 kB
	Inactive(file):  3794916 kB
	Unevictable:         128 kB
	Mlocked:             128 kB
	SwapTotal:       4193788 kB
	SwapFree:        4193788 kB
	Dirty:                40 kB
	Writeback:             0 kB
	AnonPages:        940140 kB
	Mapped:          2345880 kB
	Shmem:            769712 kB
	Slab:             349068 kB
	SReclaimable:     256692 kB
	SUnreclaim:        92376 kB
	KernelStack:        7904 kB
	PageTables:        31804 kB
	NFS_Unstable:          0 kB
	Bounce:                0 kB
	WritebackTmp:          0 kB
	CommitLimit:    20610368 kB
	Committed_AS:    5923148 kB
	VmallocTotal:   34359738367 kB
	VmallocUsed:           0 kB
	VmallocChunk:          0 kB
	HardwareCorrupted:     0 kB
	AnonHugePages:    481280 kB
	CmaTotal:              0 kB
	CmaFree:               0 kB
	HugePages_Total:       0
	HugePages_Free:        0
	HugePages_Rsvd:        0
	HugePages_Surp:        0
	Hugepagesize:       2048 kB
	DirectMap4k:      295568 kB
	DirectMap2M:     9027584 kB
	DirectMap1G:    24117248 kB

 * Give the percentage of MemFree out of MemTotal (memory not used for FS buffers)
 * or MemAvailable out of MemTotal (the above sans FS cache)
 */
char* getMemory(char* buf, int freeMem) {
	static unsigned long totKb,
						 freeKb,
						 availKb;

	readProcFile("meminfo", buf);
	strtok(buf, " ");
	totKb = strtoul(strtok(NULL, " "), NULL, 10); /* get the total memory */
	strtok(NULL, " "); /* skip kB, \n, MemFree: */
	freeKb = strtoul(strtok(NULL, " "), NULL, 10); /* get free memory */
	strtok(NULL, " "); /* skip kB, \n, MemAvailable: */
	availKb = strtoul(strtok(NULL, " "), NULL, 10); /* get available memory */

	if (0 == totKb) {
		strcpy(buf, "Mem:N/A");
	}
	else {
		sprintf(buf, "Mem:%lu%%", 100UL -
				(freeMem ? freeKb : availKb)
				* 100UL/totKb);
	}
	return buf;
}
