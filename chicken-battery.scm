(foreign-declare "#include \"battery.h\""
				 "#ifdef SYSTEMD"
				 "#include \"sdbus.h\""
				 "#endif")
				 

(define BUFSIZE 512)

(define getBatteryTime (foreign-lambda c-string "getBatteryTime" c-string))
(define getBatteryPercent (foreign-lambda c-string "getBatteryPercent" c-string))

(define sd-init
  (foreign-lambda* void ()
				   "#ifdef SYSTEMD\n"
				   "sd_init();\n"
				   "#endif"
				   ))

(define sd-free
  (foreign-lambda* void ()
				   "#ifdef SYSTEMD\n"
				   "sd_free();\n"
				   "#endif"))

(sd-init)
(print (getBatteryTime (make-string BUFSIZE)))
(print (getBatteryPercent (make-string BUFSIZE)))
(sd-free)

;(print
;  ((foreign-lambda c-string "getBatteryTime" c-string)
;   (make-string BUFSIZE)))
